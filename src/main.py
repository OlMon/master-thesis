import torch
import torch.nn as nn
from torchvision import transforms
import torch.optim as optim

from dataloader.dataloader import Heart3DSegmentationDataset
from dataloader.transform import DatasetTransformer
from nnet.heart3DSegmenter import Heart3DSegmenter
from nnet.unet21d import Unet21D
from nnet.unetHybrid23 import UnetHybrid23
from nnet.unetHybrid32 import UnetHybrid32
from nnet.unet3d import Unet3D
from nnet.unet2d import Unet2D
import os

def main():
    
    # Hyperparameter
    batch_size = 4
    epochs = 100
    patience = 0
    validation_size = 0.2

    learning_rate = 0.0003
    momentum = 0.99

    use_gpu_count = 4

    comment = '-ID:H23D-MG-MC 001 same batch as SG'

    #  Other Parameter
    to_learn = 'MULTI'  # set it to ENDO, EPI or MULTI

    seed = 42
    hflip_chance = 0.5
    vflip_chance = 0.5
    rotate_range = (-180, 180)
    rotate_chance = 0.5
    translate_range = (0, 80)
    translate_chance = 0.5
    scale_chance = 0.5
    scale_range = (0.5, 1.5)
    
    transformer = DatasetTransformer(seed, hflip_chance, vflip_chance, rotate_range, rotate_chance, translate_range, translate_chance, scale_chance, scale_range)

    mean = 252.5130
    std = 418.2402

    if(to_learn == 'MULTI'):
        classes = 3
        error_weights = [0.33792094845962206, 53.73149208523053, 45.214204501550924]
    elif(to_learn == 'ENDO'):
        classes = 2
        error_weights = [0.33541413749789944, 53.73149208523053]
    elif(to_learn == 'EPI'):
        classes = 2
        error_weights = [0.33792083973328674, 24.55370523564603]

    dataset_path = 'data/heart_scans/img.csv'

    #dataset = Heart3DSegmentationDataset(dataset_path, to_learn)
    dataset = Heart3DSegmentationDataset(dataset_path, to_learn, transformer)
    #dataset = Heart3DSegmentationDataset(dataset_path, to_learn, transformer, mean=[mean], std=[std])

    # dataset sizes TODO: set for cropping before
    channels = 1

    unet = UnetHybrid23(channels, classes=classes)
    #unet.load_state_dict(torch.load('runs/Jan11_22-51-07_quadriga-ID:3D-MG-SC 001 EPI/model_saves/best_model.pt'))
    criterion = nn.CrossEntropyLoss(weight=torch.FloatTensor(error_weights))

    segmenter = Heart3DSegmenter(unet, criterion, dataset, comment, seed)
    segmenter.use_dataparallel(use_gpu_count)

    #optimizer = optim.SGD(segmenter.net.parameters(), lr=learning_rate, momentum=momentum)
    optimizer = optim.Adam(segmenter.net.parameters())

    segmenter.train(epochs, optimizer, batch_size, validation_size, patience)


if __name__ == "__main__":
    main()
