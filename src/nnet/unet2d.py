import torch
import torch.nn as nn
from .layers import layers_2D
from .layers import layers_output


class Unet2D(nn.Module):
    """Class that puts all necessary steps for a unet together."""
    def __init__(self, channel, classes=2):
        super(Unet2D, self).__init__()

        kSize = (3, 3, 3)
        pad = (1, 1, 1)  # padding 1 so ksize 3 doesn't shorten the output
        stride = (1, 1, 1)
        pool_kSize = (1, 2, 2)
        pool_stride = (1, 2, 2)
        dropout = 0.0
        make_norm = True

        channels = [10, 20, 40, 80, 160, 320]

        self.downStep1 = layers_2D.DownStep2D(channel, channels[0], kernel_size=kSize, padding=pad, stride=stride, pool_kernel_size=pool_kSize, pool_stride=pool_stride, make_norm=make_norm)
        self.downStep2 = layers_2D.DownStep2D(channels[0], channels[1], kernel_size=kSize, padding=pad, stride=stride, pool_kernel_size=pool_kSize, pool_stride=pool_stride, make_norm=make_norm)
        self.downStep3 = layers_2D.DownStep2D(channels[1], channels[2], kernel_size=kSize, padding=pad, stride=stride, pool_kernel_size=pool_kSize, pool_stride=pool_stride, make_norm=make_norm)
        self.downStep4 = layers_2D.DownStep2D(channels[2], channels[3], kernel_size=kSize, padding=pad, stride=stride, pool_kernel_size=pool_kSize, pool_stride=pool_stride, make_norm=make_norm)
        self.downStep5 = layers_2D.DownStep2D(channels[3], channels[4], kernel_size=kSize, padding=pad, stride=stride, pool_kernel_size=pool_kSize, pool_stride=pool_stride, make_norm=make_norm)

        self.center = layers_2D.CenterStep2D(channels[4], channels[5], kernel_size=kSize, padding=pad, stride=stride, dropout=dropout, make_norm=make_norm)

        self.upStep1 = layers_2D.UpStep2D(channels[5], channels[4], kernel_size=kSize, padding=pad, stride=stride, make_norm=make_norm)
        self.upStep2 = layers_2D.UpStep2D(channels[4], channels[3], kernel_size=kSize, padding=pad, stride=stride, make_norm=make_norm)
        self.upStep3 = layers_2D.UpStep2D(channels[3], channels[2], kernel_size=kSize, padding=pad, stride=stride, make_norm=make_norm)
        self.upStep4 = layers_2D.UpStep2D(channels[2], channels[1], kernel_size=kSize, padding=pad, stride=stride, make_norm=make_norm)
        self.upStep5 = layers_2D.UpStep2D(channels[1], channels[0], kernel_size=kSize, padding=pad, stride=stride, make_norm=make_norm)

        self.output_mask = layers_output.CELOutput(channels[0], classes)

    def forward(self, batch):
        batch, batch_to_crop1 = self.downStep1(batch)
        batch, batch_to_crop2 = self.downStep2(batch)
        batch, batch_to_crop3 = self.downStep3(batch)
        batch, batch_to_crop4 = self.downStep4(batch)
        batch, batch_to_crop5 = self.downStep5(batch)

        batch = self.center(batch)

        batch = self.upStep1(batch, batch_to_crop5)
        batch = self.upStep2(batch, batch_to_crop4)
        batch = self.upStep3(batch, batch_to_crop3)
        batch = self.upStep4(batch, batch_to_crop2)
        batch = self.upStep5(batch, batch_to_crop1)

        out = self.output_mask(batch)
        return out
