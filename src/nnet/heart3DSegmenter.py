import torch
import torch.nn as nn
import torchvision
import torch.utils.data as torchData
import itertools
import time
import datetime
import numpy as np
import os
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from tensorboardX import SummaryWriter


class Heart3DSegmenter:
    """Class that gets the net and implements the training and validation."""
    def __init__(self, net, criterion, dataset, comment='', seed=0):
        print('Initializing...')
        self.gpus = 0

        self.seed = seed
        torch.manual_seed(self.seed)
        torch.cuda.manual_seed(self.seed)

        self.net = net
        self.dataset = dataset

        self.criterion = criterion
        self._criterion_weights = self.criterion.weight
        self._criterion_str = self.criterion.__str__()

        self._params = sum([p.numel() for p in self.net.parameters()])
        self._train_params = sum(p.numel() for p in self.net.parameters() if p.requires_grad)

        self.writer = SummaryWriter(comment=comment)

        # Write the graph to tensorboard (TODO: temp solution)
        s, _ = self.dataset.__getitem__(0)
        with torch.no_grad():
            self.writer.add_graph(self.net, s.view(1, s.size(0), s.size(1), s.size(2), s.size(3)))

    def _prepare_dataset(self, batch_size=1, validation_split=0.2):
        """Creates the dataloaders and splits the dataset to a training set and validation set."""
        dataset_size = len(self.dataset)
        validation_size = int(np.floor(validation_split * dataset_size))
        train_size = dataset_size - validation_size

        train_dataset, validation_dataset = torchData.random_split(self.dataset, [train_size, validation_size])

        train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
        validation_loader = torch.utils.data.DataLoader(validation_dataset, batch_size=batch_size)
        validation_loader.dataset.dataset.set_validation(True)

        return train_dataset, validation_dataset, train_loader, validation_loader

    def use_dataparallel(self, gpu_count):
        """Function to put the net and optimizer in a Dataparallel wrapper
           Args:
           gpu_count(int): Number of GPUs to use
        """
        if(gpu_count > 0 and torch.cuda.device_count() > 0):
            all_devices = list(range(0, torch.cuda.device_count()))
            devices = all_devices[:gpu_count]
            device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
            if torch.cuda.device_count() > 1:
                print("Let's use", len(devices), "GPUs!")
                self.gpus = len(devices)
                self.net = nn.DataParallel(self.net, device_ids=devices)

                self.net.to(device)
                self.criterion.to(device)
        else:
            print("Let's use CPU only!")
            self.gpus = 0

    def _calc_epoch(self, train_loader, optimizer):
        """Function to calculate one whole epoch."""
        epoch_loss = 0.0
        epoch_acc = 0.0
        self.net.train()
        train_loader.dataset.dataset.set_validation(False)
        for idx, (mini_batch, target) in enumerate(train_loader):
            optimizer.zero_grad()
            outputs = self.net.forward(mini_batch)
            if(self.gpus > 0):
                target = target.cuda()
            loss = self.criterion(outputs, target.squeeze(dim=1).long())  # remove second dimension for cross entropy loss
            loss.sum().backward()  # need to do a sum, because of the many losses from the multiple gpu
            optimizer.step()

            epoch_acc += (outputs.argmax(dim=1, keepdim=True).eq(target.long())).sum().item() / target.numel()
            epoch_loss += loss.sum().item()  # need to do a sum, because of the many losses from the multiple gpu

        return (epoch_loss / (idx + 1)), (epoch_acc / (idx + 1))

    def train(self, epochs, optimizer, batch_size=1, validation_split=0.2, patience=0):
        """Function to train the model.
           Args:
               epochs(int): How many epoch should be trained.
               optimizer(pytorch.optimizer): Optimizer to use during training.
               gpu_count(int): On how many GPUs the net should train.
               batch_size(int): What batch size to use during training.
               validation_split(float): How much of the dataset use for validation
               patience(int): Patience for ealry stopping. (0 = No ealry stopping)"""
        self.train_dataset, self.validation_dataset, self.train_loader, self.validation_loader = self._prepare_dataset(batch_size, validation_split)

        self.write_start_summary(self.writer, self.net, self.dataset, self.train_dataset, self.validation_dataset,
                                 self._criterion_str, self._criterion_weights, optimizer, batch_size, self.gpus,
                                 epochs, patience, self.seed, self._params, self._train_params)

        min_loss = float('inf')
        min_loss_epoch = 0
        cur_patience = 0
        start_time = time.time()

        for epoch in range(epochs):
            try:
                print('Starting %d. Epoch' % (epoch + 1))
                train_loss, train_acc = self._calc_epoch(self.train_loader, optimizer)
                # After calculating an epoch, validate for taking statistics
                validation_loss, validation_acc = self.validate(self.validation_loader, epoch)

                self.writer.add_scalars("data/loss", {"training_loss": train_loss, "validation_loss": validation_loss}, epoch)
                self.writer.add_scalars("data/acc", {"training_acc": train_acc, "validation_acc": validation_acc}, epoch)

                print('Calculated epoch %d from %d' % (epoch+1, epochs))

                # early stopping implementation
                if((min_loss > validation_loss)):
                    min_loss = validation_loss
                    min_loss_epoch = epoch
                    cur_patience = 0
                    self.save_net(self.net, self.writer.file_writer.get_logdir(), 'best_model')
                else:
                    if(patience > 0):
                        cur_patience += 1
                        if(cur_patience == patience):
                            finish_reason = 'Training finished because of early stopping'
                            break

            except KeyboardInterrupt:
                # In case the user wants to interrupt the training
                finish_reason = 'Training interrupted by user'
                break
        else:
            finish_reason = 'Training finished normally'

        self.write_end_summary(self.writer, start_time, epoch+1, train_loss, validation_loss, min_loss, min_loss_epoch, finish_reason)
        self.writer.close()
        print('Finished Training')

    def validate(self, validation_loader, epoch):
        """Function to validate the net.
           Args:
               criterion(method): Function to calculate the loss."""
        self.net.eval()
        valid_loss = 0.0
        valid_acc = 0.0
        conf_matrix = None
        validation_loader.dataset.dataset.set_validation(True)
        with torch.no_grad():
            for idx, (scan, target) in enumerate(validation_loader):
                if conf_matrix is None:
                    conf_matrix = np.zeros((target.size(1), target.size(1)))

                output = self.net.forward(scan)
                if(self.gpus > 0):
                    target = target.cuda()

                loss = self.criterion(output, target.squeeze(dim=1).long())

                valid_loss += loss.sum().item()
                valid_acc += (output.argmax(dim=1, keepdim=True).eq(target.long())).sum().item() / target.numel()

                # Compute confusion matrix and add it to the running confusion matrix
                conf_matrix = np.add(confusion_matrix(target.view(-1).cpu().numpy(), output.argmax(dim=1, keepdim=True).view(-1).cpu().numpy()), conf_matrix)

                # save images in the writer but only first image of first batch
                if(idx == 0):
                    pred_mask = output.argmax(dim=1, keepdim=True)
                    pred_mask = pred_mask.type(torch.FloatTensor) * (scan.max() / 2)
                    target = target.type(torch.FloatTensor) * (scan.max() / 2)
                    img = torch.cat((scan, pred_mask.type(torch.FloatTensor), target), 2)
                    img = img[0].permute(1, 0, 2, 3)
                    img_row = torchvision.utils.make_grid(img, nrow=10, padding=2, normalize=True, pad_value=1)

                    self.writer.add_image("masks/conc", img_row, epoch)

            fig = plt.figure()
            self.plot_confusion_matrix(conf_matrix, len(conf_matrix))
            self.writer.add_figure('conv_matrix', fig, epoch)

        return(valid_loss / (idx + 1)), (valid_acc / (idx + 1))

    def plot_confusion_matrix(self, cm, classes,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
        """Function to plot an matplotlib figure representing the confusion matrix.
           Args:
                cm(numpy array): Confusion matrix
                classes(int): How many classes are there
                title(string): Title to show in the plot
                cmap(matplotlib.ColorMap): Colormap to use in the figure"""

        cmn = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

        plt.imshow(cmn, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        plt.clim(0.0, 1.0)
        tick_marks = np.arange(classes)
        plt.xticks(tick_marks, range(classes))
        plt.yticks(tick_marks, range(classes))

        thresh = cmn.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, format(cmn[i, j], '.4f') + '\n' + str(int(cm[i, j])),
                     horizontalalignment="center",
                     color="white" if cmn[i, j] > thresh else "black")

        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.tight_layout()

    def write_start_summary(self, writer, net, dataset, train_dataset, validation_dataset,
                            criterion_str, criterion_weight, optimizer, batch_size,
                            gpu_cnt, epochs, patience, seed, params, train_params):
        """Function to write all start parameters to Tensorboardx."""

        net_str = "{0}".format(net).replace("\n", "  \n\n").replace("    ", "\t\t").replace("  ", "\t")

        dataset_str = """{0} \n Split into:  \n
        {1} training set size  \n
        {2} validation set size.""".format(dataset, len(train_dataset), len(validation_dataset))

        criterion_str = """Loss function:  \n
        {0}  \n
        Error weights: {1}""".format(criterion_str, criterion_weight)

        optimizer_str = """Optimizer: {0}""".format(optimizer).replace(')','').replace('(','').replace("\n", "  \n\n").replace('    ', '  \t\t')

        training_str = """Training settings:  \n
        Batch size: {0}  \n
        GPU count: {1}  \n
        Epochs: {2}  \n
        Early stopping: {3} \n
        Torch seed: {4}""".format(batch_size, gpu_cnt, epochs, patience, seed)

        params_str = """Number of parameters in the model:  \n
        Total: {0}  \n
        Trainable: {1}""".format(params, train_params)

        writer.add_text('params', dataset_str)
        writer.add_text('params', criterion_str)
        writer.add_text('params', optimizer_str)
        writer.add_text('params', training_str)
        writer.add_text('net', net_str)
        writer.add_text('net', params_str)

    def write_end_summary(self, writer, start_time, epoch, training_loss,
                          validation_loss, min_validation,
                          min_validation_epoch, finish_reason):
        """Function to write all end results to Tensorboardx."""

        output_str = """End Results:  \n
        Runtime: {0}  \n
        Epochs calculated: {1}  \n
        Final training loss: {2}  \n
        Final validation loss: {3} \n
        Best validation loss: {4} \n
        Best validation loss in epoch: {5} \n
        Finish reason: {6}""".format(str(datetime.timedelta(seconds=(time.time() - start_time))), epoch, training_loss, validation_loss, min_validation, min_validation_epoch, finish_reason)

        writer.add_text('end', output_str)

    def save_net(self, model, path, filename):
        path = os.path.join(path, 'model_saves')
        if (not os.path.exists(path)):
            os.makedirs(path)
        try:
            state_dict = model.module.state_dict()
        except AttributeError:
            state_dict = model.state_dict()
        torch.save(state_dict, os.path.join(path, filename+'.pt'))
