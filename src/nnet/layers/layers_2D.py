import torch
import torch.nn as nn
import torch.nn.functional as torchFunc


class Step2D(nn.Module):
    """Class that wrappes a convolution, batchnorm and relu together."""
    def __init__(self, in_channels, out_channels, kernel_size, padding, stride, make_norm=True):
        super(Step2D, self).__init__()
        self.conv = nn.Conv3d(in_channels=in_channels, out_channels=out_channels, kernel_size=(1, kernel_size[1], kernel_size[2]), padding=(0, padding[1], padding[2]), stride=(1,stride[1], stride[2]))
        self.make_norm = make_norm
        if(self.make_norm):
            self.batchNorm = nn.BatchNorm3d(out_channels)
        self.relu = nn.ReLU()

    def forward(self, batch):
        batch = self.conv(batch)
        if(self.make_norm):
            batch = self.batchNorm(batch)
        batch = self.relu(batch)

        return batch


class DownStep2D(nn.Module):
    """Class representing a downstep, according to the original unet implementation."""
    def __init__(self, in_channels, out_channels, kernel_size, padding, stride, pool_kernel_size, pool_stride, make_norm=True):
        super(DownStep2D, self).__init__()
        self.step1 = Step2D(in_channels, out_channels, kernel_size, padding, stride, make_norm)
        self.step2 = Step2D(out_channels, out_channels, kernel_size, padding, stride, make_norm)
        self.maxPool = nn.MaxPool3d(kernel_size=pool_kernel_size, stride=pool_stride)

    def forward(self, batch):
        batch = self.step1(batch)
        batch = self.step2(batch)
            
        batch_to_crop = batch

        batch = self.maxPool(batch)

        return batch, batch_to_crop


class CenterStep2D(nn.Module):
    """Class representing a downstep, according to the original unet implementation."""
    def __init__(self, in_channels, out_channels, kernel_size, padding, stride, dropout=0, make_norm=True):
        super(CenterStep2D, self).__init__()
        self.step1 = Step2D(in_channels, out_channels, kernel_size, padding, stride, make_norm)
        self.step2 = Step2D(out_channels, out_channels, kernel_size, padding, stride, make_norm)
        self.dropout = nn.Dropout3d(dropout)

    def forward(self, batch):
        batch = self.step1(batch)
        batch = self.dropout(batch)
        batch = self.step2(batch)

        return batch


class UpStep2D(nn.Module):
    """Class representing a upstep, according to the original unet implementation."""
    def __init__(self, in_channels, out_channels, kernel_size, padding, stride, make_norm=True):
        super(UpStep2D, self).__init__()
        self.step1 = Step2D(in_channels, out_channels, kernel_size, padding, stride, make_norm)
        self.step2 = Step2D(out_channels, out_channels, kernel_size, padding, stride, make_norm)


    def forward(self, batch, batch_to_crop):
        batch = torchFunc.interpolate(batch, (batch_to_crop.size(2), batch_to_crop.size(3), batch_to_crop.size(4)), scale_factor=None, mode='nearest')
        batch = self.step1(batch)
        batch = torch.cat((batch, batch_to_crop), 1) # concatonate over the channel dimension

        batch = self.step1(batch)
        batch = self.step2(batch)
        
        return batch
