import torch.nn as nn


class CELOutput(nn.Module):
    """Class for last layer, for a  multi class segmentation."""
    def __init__(self, in_channels, class_amount):
        super(CELOutput, self).__init__()
        self.out_layer = nn.Conv3d(in_channels, class_amount, kernel_size=1, padding=0, stride=1)
        
    def forward(self, batch):
        return self.out_layer(batch)
