import torch
import torch.nn as nn
import torch.nn.functional as torchFunc


class Step21D(nn.Module):
    """Class that represents a 2+1D convolution. It expects a kernel of size (d,h,w),
       and uses a 2D convolution with kernel (1,h,w) and a 1D convolution with kernel (d,1,1)"""
    def __init__(self, in_channels, out_channels, kernel_size, padding, stride, make_norm=True):
        super(Step21D, self).__init__()
        self.conv2d = nn.Conv3d(in_channels=in_channels, out_channels=out_channels, kernel_size=(1,kernel_size[1],kernel_size[2]), padding=(0, padding[1], padding[2]), stride=stride)
        self.make_norm = make_norm
        if(self.make_norm):
            self.batchNorm = nn.BatchNorm3d(out_channels)
        self.relu = nn.ReLU()

        self.conv1d = nn.Conv3d(in_channels=out_channels, out_channels=out_channels, kernel_size=(kernel_size[0],1,1), padding=(padding[0], 0, 0), stride=stride)

    def forward(self, batch):
        batch = self.conv2d(batch)
        if(self.make_norm):
            batch = self.batchNorm(batch)
        batch = self.relu(batch)

        batch = self.conv1d(batch)
        if(self.make_norm):
            batch = self.batchNorm(batch)
        batch = self.relu(batch)
        
        return batch


class DownStep21D(nn.Module):
    """Class representing a downstep, with two 2+1D layers."""
    def __init__(self, in_channels, out_channels, kernel_size, padding, stride, pool_kernel_size, pool_stride, make_norm=True):
        super(DownStep21D, self).__init__()
        self.step1 = Step21D(in_channels, out_channels, kernel_size, padding, stride, make_norm)
        self.step2 = Step21D(out_channels, out_channels, kernel_size, padding, stride, make_norm)
        self.maxPool = nn.MaxPool3d(kernel_size=pool_kernel_size, stride=pool_stride)

    def forward(self, batch):
        batch = self.step1(batch)
        batch = self.step2(batch)
            
        batch_to_crop = batch

        batch = self.maxPool(batch)

        return batch, batch_to_crop


class CenterStep21D(nn.Module):
    """Class representing a downstep, according to the original unet implementation."""
    def __init__(self, in_channels, out_channels, kernel_size, padding, stride, dropout=0, make_norm=True):
        super(CenterStep21D, self).__init__()
        self.step1 = Step21D(in_channels, out_channels, kernel_size, padding, stride, make_norm)
        self.step2 = Step21D(out_channels, out_channels, kernel_size, padding, stride, make_norm)
        self.dropout = nn.Dropout3d(dropout)

    def forward(self, batch):
        batch = self.step1(batch)
        batch = self.dropout(batch)
        batch = self.step2(batch)

        return batch


class UpStep21D(nn.Module):
    """Class representing an upstep, with two 2+1D layers."""
    def __init__(self, in_channels, out_channels, kernel_size, padding, stride, make_norm=True):
        super(UpStep21D, self).__init__()
        self.step1 = Step21D(in_channels, out_channels, kernel_size, padding, stride, make_norm)
        self.step2 = Step21D(out_channels, out_channels, kernel_size, padding, stride, make_norm)


    def forward(self, batch, batch_to_crop):
        batch = torchFunc.interpolate(batch, (batch_to_crop.size(2), batch_to_crop.size(3), batch_to_crop.size(4)), scale_factor=None, mode='nearest')
        batch = self.step1(batch)
        batch = torch.cat((batch, batch_to_crop), 1) # concatonate over the channel dimension

        batch = self.step1(batch)
        batch = self.step2(batch)
        
        return batch
