import glob
import torch
from PIL import Image
import torch.utils.data as data
import numpy as np
import pandas as pd
import os
import re
import csv

#script_path = os.path.dirname(os.path.realpath(__file__))

# for quick testing in emacs
script_path = '/home/mpa/Workplace/Thesis/src/data/heart_scans'

img_paths = glob.glob(script_path + '/**/*ENDO*.png', recursive=True)
img_names = []
for img_path in img_paths:
    img_names.append(re.sub(r'^(.*?)/', '', img_path.replace(script_path + '/', '')))

cnt = 0
 
with open('img.csv', 'w') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    filewriter.writerow(['Patient', 'ScanType', 'Time', 'Slice', 'Path'])
    while(cnt < len(img_names)):
        row = img_names[cnt].split('_')[:-1]
        row.append(img_paths[cnt].replace('ENDO', '<TYPE>'))
        filewriter.writerow(row)
        cnt += 1
