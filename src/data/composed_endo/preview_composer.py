import numpy as np
from PIL import Image
import glob
import os
import re


script_path = '/home/mpa/Workplace/Thesis/src/data/heart_scans'

img_paths = glob.glob(script_path + '/**/*ENDO*.png', recursive=True)

heart_conv_val = 0.1
ep_conv_val = 65535
en_conv_val = 65535
trans_rate = 30

for img_path in img_paths:
    endo_path = img_path
    epi_path = img_path.replace('ENDO', 'EPI')
    orig_path = img_path.replace('ENDO', 'ORIG')

    img_endo = Image.open(endo_path) 
    img_epi  = Image.open(epi_path)
    img_orig = Image.open(orig_path)

    img_endo = img_endo.point(lambda i:( i * en_conv_val)).convert("RGBA")
    img_epi = img_epi.point(lambda i:( i * ep_conv_val)).convert("RGBA")
    img_orig = img_orig.point(lambda i:( i * heart_conv_val)).convert("RGB")
    
    s = img_endo.size

    for x in range(0,s[0]):
        for y in range(0,s[1]):
            cur_color = img_endo.getpixel((x,y))
            if cur_color[0] > 0:
                new_color = (cur_color[0] ,0, 0, trans_rate)
            else:
                new_color = (cur_color[0], cur_color[1], cur_color[2], 0)
            img_endo.putpixel((x,y), new_color)

    s = img_epi.size

    for x in range(0,s[0]):
        for y in range(0,s[1]):
            cur_color = img_epi.getpixel((x,y))
            if cur_color[0] > 0:
                new_color = (0, cur_color[1], 0, trans_rate)
            else:
                new_color = (cur_color[0], cur_color[1], cur_color[2], 0)
            img_epi.putpixel((x,y), new_color)

    s = img_epi.size

    for x in range(0,s[0]):
        for y in range(0,s[1]):
            if img_endo.getpixel((x,y))[3] > 0:
                new_color = (img_epi.getpixel((x,y))[0], img_epi.getpixel((x,y))[1], img_epi.getpixel((x,y))[2], 0)
            else:
                new_color = (img_epi.getpixel((x,y))[0], img_epi.getpixel((x,y))[1], img_epi.getpixel((x,y))[2], img_epi.getpixel((x,y))[3])
            img_epi.putpixel((x,y), new_color)

    #img_orig.paste(img_epi, (0,0), img_epi)
    img_orig.paste(img_endo, (0,0), img_endo)
    
    filename = os.path.dirname(os.path.realpath(__file__))+ '/' + re.sub(r'^(.*?)/', '', img_path.replace(script_path + '/', '')).split('_')[0] + '/' + re.sub(r'^(.*?)/', '', img_path.replace(script_path + '/', ''))
    filename = filename.replace("ENDO", "ENDO_COMPOSED")
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    img_orig.save(filename)
    
