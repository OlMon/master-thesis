import torch
from PIL import Image
import torch.utils.data as data
from torchvision import transforms
import torchvision.transforms.functional as TF
import pandas as pd


class Heart3DSegmentationDataset(data.Dataset):
    """Dataset for heartsegmentation in 3d"""

    def __init__(self, csv_file, mask_type, transformer=None, mean=None, std=None):
        """
        Args:
            csv_file(string): Path to csv file with all endo images
            mask_type(string): What to load, ENDO, EPI or MULTI files
            transform(callable, optional): Optional tranformation
        """
        self.csv_file = csv_file
        self.mask_type = mask_type
        self.transformer = transformer
        self.mean = mean
        self.std = std
        self.set_validation(False)
        df = pd.read_csv(self.csv_file)
        self.gr = df.groupby(['Patient', 'Time'])
        self.gr = self.gr.Path.apply(list)

    def set_validation(self, state):
        self.validationset = state

    def __str__(self):
        return """Dataset:  \n
        Size: {0}  \n
        Mask-Type: {1}  \n
        Used mean: {2}  \n
        Used standard deviation: {3}  \n
        Transformer used:  \n {4}""".format(self.__len__(), self.mask_type, self.mean, self.std, self.transformer)
        
    def __len__(self):
        """Returns the number of data in the dataloader."""
        return len(self.gr)

    def __getitem__(self, idx):
        """
        Gets the item by its index.
        Args:
            idx(int): Index of item to get."""

        # get raw paths in sorted order
        raw_dirs = sorted(self.gr[idx])

        orig_imgs = []
        endo_imgs = []
        epi_imgs  = []

        for raw_dir in raw_dirs:
            orig_dir = raw_dir.replace('<TYPE>', 'ORIG')
            endo_dir = raw_dir.replace('<TYPE>', 'ENDO')
            epi_dir  = raw_dir.replace('<TYPE>', 'EPI')
            orig_imgs.append(Image.open(orig_dir))
            endo_imgs.append(Image.open(endo_dir))
            epi_imgs.append(Image.open(epi_dir))

        resizer = transforms.Compose([transforms.RandomCrop(320, pad_if_needed=True)])

        # apply resizing
        for idx, orig_img in enumerate(orig_imgs):
            orig_imgs[idx] = resizer(orig_img)

        if((self.mask_type == 'EPI') or (self.mask_type == 'MULTI')):
            for idx, epi_img in enumerate(epi_imgs):
                epi_imgs[idx] = resizer(epi_img)

        if((self.mask_type == 'ENDO') or (self.mask_type == 'MULTI')):
            for idx, endo_img in enumerate(endo_imgs):
                endo_imgs[idx] = resizer(endo_img)

        # Transforms the images randomly with the given transformer
        if(self.transformer and not(self.validationset)):
            if(self.mask_type == 'EPI'):
                orig_imgs, epi_imgs, _ = self.transformer.transform(orig_imgs, epi_imgs)
            if(self.mask_type == 'ENDO'):
                orig_imgs, endo_imgs, _ = self.transformer.transform(orig_imgs, endo_imgs)
            if(self.mask_type == 'MULTI'):
                orig_imgs, epi_imgs, endo_imgs = self.transformer.transform(orig_imgs, epi_imgs, endo_imgs)

        orig_tensors = []
        endo_tensors = []
        epi_tensors  = []

        # Normalize the scans and transform them to tensors
        for orig_img in orig_imgs:
            t = TF.to_tensor(orig_img)
            if(self.mean and self.std):
                t = TF.normalize(t, self.mean, self.std)
            orig_tensors.append(t.type(torch.FloatTensor))

        # Transforms masks to tensors
        if((self.mask_type == 'EPI') or (self.mask_type == 'MULTI')):
            for epi_img in epi_imgs:
                epi_tensors.append(TF.to_tensor(epi_img).type(torch.FloatTensor))

        # Transforms masks to tensors
        if((self.mask_type == 'ENDO') or (self.mask_type == 'MULTI')):
            for endo_img in endo_imgs:
                endo_tensors.append(TF.to_tensor(endo_img).type(torch.FloatTensor))

        # Stack the tensors to create 4D Tensors (C, D, W, H)
        scan = torch.stack(orig_tensors, dim=1)

        if(self.mask_type == 'EPI'):
            mask = torch.stack(epi_tensors, dim=1)

        if(self.mask_type == 'ENDO'):
            mask = torch.stack(endo_tensors, dim=1)

        if(self.mask_type == 'MULTI'):
            for idx in range(len(epi_tensors)):
                epi_tensors[idx] = (((epi_tensors[idx] - endo_tensors[idx]) == 1) * 2).type(torch.FloatTensor)
            mask = torch.stack(endo_tensors, dim=1).add(torch.stack(epi_tensors, dim=1))

        return scan, mask
