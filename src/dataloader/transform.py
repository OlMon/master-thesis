import torch
import random
from PIL import Image
import torchvision.transforms.functional as TF


class DatasetTransformer(object):
    """Class to transform 3D images and scan."""

    def __init__(self, seed, hflip_chance, vflip_chance, rotate_range, rotate_chance, translate_range, translate_chance, scale_chance, scale_range):
        self.seed = seed
        random.seed(self.seed)
        self.hflip_chance = hflip_chance
        self.vflip_chance = vflip_chance
        self.rotate_chance = rotate_chance
        self.rotate_range = rotate_range  # maximal -180 - 180
        self.translate_chance = translate_chance
        self.translate_range = translate_range
        self.scale_chance = scale_chance
        self.scale_range = scale_range


    def __str__(self):
        return """Transformer:  \n
        Seed: {0}  \n
        Horizontal flip chance: {1}  \n
        Vertical flip chance: {2}  \n
        Rotation chance: {3}  \n
        Rotation range(min, max): {4}  \n
        Translation chance: {5}  \n
        Translation range(min, max): {6}  \n
        Scaling chance: {7}  \n
        Scaling range(min, max): {8}""".format(self.seed, self.hflip_chance, self.vflip_chance, self.rotate_chance, self.rotate_range, self.translate_chance, self.translate_range, self.scale_chance, self.scale_range)
        
    def transform(self, scans, first_mask, second_mask=None):
        """Transforms a list of 3D images (represented as array of images) at the same rate"""
        if(random.random() < self.rotate_chance):
            rot = random.randrange(self.rotate_range[0], self.rotate_range[1])
        else:
            rot = 0

        if(random.random() < self.translate_chance):
            transh = random.randint(self.translate_range[0], self.translate_range[1])
            transv = random.randint(self.translate_range[0], self.translate_range[1])
        else:
            transh = 0
            transv = 0

        if(random.random() < self.scale_chance):
            scale = random.uniform(self.scale_range[0], self.scale_range[1])
        else:
            scale = 1

        flipv = random.random() < self.vflip_chance
        fliph = random.random() < self.hflip_chance

        for idx, img in enumerate(scans):
            img = TF.affine(img, rot, (transh, transv), scale, 0)
            if(flipv):
                img = TF.vflip(img)
            if(fliph):
                img = TF.hflip(img)
            scans[idx] = img

        for idx, img in enumerate(first_mask):
            img = TF.affine(img, rot, (transh, transv), scale, 0)
            if(flipv):
                img = TF.vflip(img)
            if(fliph):
                img = TF.hflip(img)
            first_mask[idx] = img

        if(second_mask):
            for idx, img in enumerate(second_mask):
                img = TF.affine(img, rot, (transh, transv), scale, 0)
                if(flipv):
                    img = TF.vflip(img)
                if(fliph):
                    img = TF.hflip(img)
                second_mask[idx] = img

        return scans, first_mask, second_mask
