import torch
import numpy as np

def error_weights_calc(dataloader):
    """Function to calculate error weights of a givin dataset.
       Parameter:
       dataloader(torch.dataloader): Dataloader of the dataset
to calculate the error weights."""
    sum_pix = 0
    fst_class = 0
    snd_class = 0
    thr_class = 0
    for _, m in dataloader:
        sum_pix += m.numel()
        
        fst_class += len((m == 0).nonzero())
        snd_class += len((m == 1).nonzero())
        thr_class += len((m == 2).nonzero())

    fst_class_frq = fst_class / sum_pix
    snd_class_frq = snd_class / sum_pix
    thr_class_frq = thr_class / sum_pix

    median_frq = np.mean(np.array([fst_class_frq, snd_class_frq, thr_class_frq]))
        
    print('{0}, {1}, {2}'.format((median_frq / fst_class_frq), (median_frq / snd_class_frq), (median_frq / thr_class_frq)))

