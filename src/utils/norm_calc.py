


def mean_std_calc(dataloader):
    """Function to calculate the mean and standard
       deviation of a givin dataset.
       Parameter:
       dataloader(torch.dataloader): Dataloader of the dataset
to calculate the mean and standard deviation.
Inspired from 'ptrblck' at https://discuss.pytorch.org/t/about-normalization-using-pre-trained-vgg16-networks/23560/6"""
    mean = 0
    std = 0
    samples = 0
    for data, _ in dataloader:
        batch_samples = data.size(0)
        data = data.view(batch_samples, data.size(1), -1)
        mean += data.mean(2).sum(0)
        std += data.std(2).sum(0)
        samples += batch_samples

    return (mean / samples),(std / samples)
