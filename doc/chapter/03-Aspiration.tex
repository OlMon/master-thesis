\section{CNN based semantic segmentation}
\label{cha:state-of-the-art}
After presenting in the previous chapter the fundamental information about the medical and NN background, this chapter shows some recent work in image segmentation. These state of the art projects represent common usage of CNNs in image segmentation, with some are being used with medical images.

\subsection{U-Net}
\label{sec:unet}
Ronneberger et al. created a NN architecture called U-Net~\cite{DBLP:journals/corr/RonnebergerFB15}.

The name U-Net comes from the topology of the network itself. U-Net consist of two paths.
The first contracting path learns to capture the context, where as the symmetrical expanding path enables the precise localization. A visualization of the network is presented in figure~\ref{pic:orig_unet}.

Every step of the contracting path consists of two convolution layers, with a filter size of 3x3. As activation function after the convolutions rectifier linear unit (ReLU) was used.

The down step itself was a 2x2 maxpool, decreasing the featuremaps size by half. Using maxpool in the down step limits the maximal applicable down steps in a U-Net.

The partial result after every step of the contracting path is saved, cropped and merged with the corresponding partial result of the expanding path. These are called skip connections.

The expanding path is similar to the contracting path. The steps consisting of two convolution layers with ReLU activation. The change is that instead a maxpool an up-convolution is used. This up-convolution is to increase the size of the featuremaps by upsampling them and applying a 2x2 convolution.

After the completed expanding path a 1x1 convolution is used to create an output with the desired depth.

Because of the use of convolution layers with kernels of size 3x3, the resulting masks were smaller than the input data. These kernel sizes were also the cause of the cropping in the skip connections.

The smaller mask size and the necessity of cropping, what can be considered a loss of information, were two downsides of this approach.

\begin{figure}[h]
  \frame{\includegraphics[width=0.8\textwidth]{pictures/orig_unet.png}}
  \centering
  \caption[Original U-Net]{The original U-Net, a 2D input is given, every step of the contracting path includes two2D convolutions, the results creates the skip connection to the expanding path. After a downstep maxpool is applied. For the expanding path the result of the skip connection is cropped and merged together with the up-convolution that is applied before each upstep. The final convolution is 1x1 just to reduce the featuremap size.}
  \label{pic:orig_unet}
\end{figure}

One general problem using NN in medical image processing is the relatively small available dataset compared to common dataset. AlexNet~\cite{NIPS2012} that won the ImageNet Large Scale Visual Recognition Challenge and was revolutionary for image processing with NN. The used dataset consisted of 15 million labeled images.

The MNIST dataset that is often used in training of systems to recognize handwritten digits, contains 70 thousand labeled images.

The U-Net network relies strongly on augmentation of the dataset. Ronneberger used his U-Net for segmentation of electro microscopic recording of neuronal structures. The augmentation mainly used by Ronneberger on such a dataset was shifting and rotation.
With that, the smaller dataset can be expanded and used effectively.

Additionally can augmentation prevent overfitting and a general better generalization~\cite{DBLP:journals/corr/abs-1806-03852}.
 
This U-Net was developed for 2D images, as in this work the dataset are 3D MR scans an other approach has to used. Ronneberger and a colleague of his, evolved the 2D U-Net to be used with 3D data. During the development of a new model, other additional improvements were include in the newer 3D U-Net, that could be of benefit in the segmentation of the LV. The 3D U-Net is described in the next section. 


\subsection{3D U-Net}
\label{sec:3d-unet}

Cicek created, together with Ronneberger, a 3D version of an U-Net~\cite{DBLP:journals/corr/CicekALBR16}.

This network should segment the volumetric data of xenopus kindeys. The first approach was to have some of the slices manually labeled. The partially labeled images were fed to the network to create the rest of the segmentation.

For augmentation Cicek used rotation, scaling and gray value augmentation. That was necessary as only three samples were available for the training. Each sample was of size around $245 \times 245 \times 60$ with around 77 slices that were already labeled.

This model includes some new features, that were not included in the original U-Net, besides the change in the dimension of the convolution.

In this U-Net Cicek used batch normalization after the convolutional layers.

The original U-Net did not included this, because Ioffe and Szegedy presented their paper~\cite{DBLP:journals/corr/IoffeS15} about batch normalization and the benefits for neural network, after the release of the U-Net.

Batch normalization can increase the convergence of a NN, by normalizing the output after every convolutional layer.


\begin{figure}[h]
  \frame{\includegraphics[width=0.8\textwidth]{pictures/3d_unet.png}}
  \centering
  \caption[3D U-Net]{3D U-Net with CT images as input, using 3D convolutions, with batch normalization and ReLU activation function in every step. With skip connections between the contracting and expanding path. The downsteps uses maxpooling to decrease the featuremap size, while the expanding path up-convolution uses, to increase the featuremap.}
  \source{~\cite{DBLP:journals/corr/abs-1803-08691}}
  \label{pic:3d_unet}
\end{figure}

A further change in the network was to, not only have a binary prediction, but the resulting mask calculated for every pixel an amount values equal to the amount of classes. Each value can be interpreted as a probability that this pixel is of this class. This allows to use one network that can make a multi-class prediction.

The use of a 3D U-Net with 3D Data caused a very high computational cost. The roughly 19 million trainable parameters caused a very small batch size, were as a bigger batch size with batch normalization can further increase the convergence of the network~\cite{DBLP:journals/corr/IoffeS15}.

With an additional experiment using the same U-Net Cicek tried to fully segment the data without any previously manually labeled slices. This network achieved worse result then the segmentation with partially segmented layers.

For comparison Cicek tested the results of the 3D U-net segmenting the xenopus kidney without any manual labeling against a 2D U-Net.

The 3D U-Net achieved much better results than the 2D U-Net, that evaluated each slice separately.


Besides 2D or 3D U-Nets and losing spatial information or being limited by computational costs, some are using both approaches and trying to find a middle ground.


\subsection{Hybrid Network}
\label{sec:hybrid}

2D network can process 3D data like MR images or CT scans, by evaluating every slice of the data on its own. This comes with the cost of losing the information in the depth dimension. 3D network are capable to use the whole spatial information but the computational costs are increased dramatically.

There are approaches to combine 2D convolutions with 3D convolutions creating a hybrid net.

\subsubsection{3D into 2D Hybrid}

Chang and his research~\cite{Chang} group created a network based on the feature pyramid network~\cite{DBLP:journals/corr/LinDGHHB16}.

The input data consisted of CT scans of human brains, with dimensions of 512x512x5. The goal was to segment hemorrhage inside the brain.

To achieve this goal a 2D/3D hybrid network was developed. It was based on the mask R-CNN~\cite{DBLP:journals/corr/HeGDG17}, creating a bounding box and segmentation. 

The first layers consisted of 3D convolutions and 3D operations (see~\ref{pic:chang_net}).
One step consisted of a 3D convolutional layer that used a kernel with sizes $3 \times 3$ followed by a batch normalization, a ReLU activation and lastly a 3D convolution again.

After five steps of the 3D operations the network uses 2D convolutions and operations. This limits the number of parameters in the next steps, but uses the whole spatial information in the contracting path.

The results were compared to previous techniques used for segmentation, like decision tree analysis and level-set methods both achieving worse results then the neural network.

As in~\ref{sec:unet} stated the contracting path captures the context, so there is benefit to include the whole spatial information. The expanding path enables the precise location. For the localization in two dimensional space (every slice of the CT scan) the whole three dimensional context is used.

Similar to the U-Net this network used first 3D operations to capture the whole spatial context of the input data.
After extracting the context, the exact localization could be determine on a single slice basis by using 2D operations.

\begin{figure}[h]
  \frame{\includegraphics[width=\textwidth]{pictures/hybrid32.png}}
  \centering
  \caption[Hybrid 3D/2D net]{Hybrid 3D/2D net. In the upper layers the model uses 3D operations, while using in the lower layers 2D operations.}
  \label{pic:chang_net}
\end{figure}


\subsubsection{2D into 3D Hybrid}

An other approach of a hybrid net was created by Li et al..
Other then Chang, Li used first 2D operations and 3D operations after it.
According to Li: ``hybrid densely connected UNet (H-DenseUNet), which consists of a 2D DenseUNet for efficiently extracting intra-slice features and a 3D counterpart for hierarchically aggregating volumetric contexts''~\cite{DBLP:journals/corr/abs-1709-07330}.

Li's goal was to segment the human liver from a CT scans. Additionally to the liver segmentation, liver cancer cells should be segmented.

The LiTS dataset, that was used in Li's project, comes from the \textit{liver tumor segmentation challenge}. The training dataset contained 130 CT scans, being a relatively big dataset in the medical field.

This method ranked 1st in competitive lesion segmentation in 2017. In this competition an 2D U-Net was presented. This achieved worse results. 

On an the 3DIRCADb Dataset, a dataset of medical images with manual segmentations of various structures of interest, the model achieved state of the art results.


\begin{figure}[H]
  \frame{\includegraphics[width=0.8\textwidth]{pictures/hybrid23.png}}
  \centering
  \caption[H-DenseUNet]{H-DenseUNet uses at first a 2D DenseUNet, combining the results to use the concatenated  layers for the 2D DenseUNet.}
  \label{pic:li_net}
\end{figure}

After presenting some state of the art approaches for segmentation of medical images with CNNs, the following chapter shows some drawbacks that exists to create a conclusion, what approach would be favorable for the segmentation of the LV.  

\subsection{Drawbacks}

There are a lot of different approaches for segmentation of medical images. Especially for 3D images like CT and MR images, the are many possible variations to create a model. Fully 2D without using the context of one dimension, 3D with using the full context with high computational costs or even hybrid methods, that differ in the order of 2D and 3D layers.

All described approaches achieved state of the art results, even winning competitions and showed that the models could even be used with different kind of datasets.

But a conclusion of how important the full spatial information in comparison to runtime and accuracy will be for a LV segmentation, can not be made for many reasons.

These described models used different datasets, from liver and brain CT to xenopus kindeys. Even the general structure of the models differ heavily. Some tasks were single-class segmentations other multi-class. The effects of parallelization was not tested. It is to assume that during development of the models, the research groups optimized their models according to their given task. 

For a valid comparison and reaching a conclusion as to what approach would be the best for LV segmentation, different models have to be developed and evaluated on a common dataset, while using the same hardware.
This will give a good insight for further research in creating an optimal model.

The models, techniques and approaches that were compared to each other are described in the following section. 

\newpage
\section{Aim of this Thesis}
\label{sec:aim}
This thesis presents CNNs used with different medical segmentation approaches, evaluates and compares them to each other for the use of segmentation of the epicardium and endocardium of the LV.

The comparison should show the benefits in accuracy of using the whole spatial dimension of MR images instead of using the individual layers of their own.
Additionally evaluating the computational cost and runtime of these approaches.
Further comparing the results of these approaches by segmenting the epicardium and endocardium simultaneously or individually. With multiple GPUs the effects of parallel processing of neural network and the influence on the accuracy and runtime will be taken as final comparison.
For a summary of all comparisons see following table~\ref{tab:comparisons}:

\begin{table}[H]
\begin{center}
\begin{tabular}{l|llll}
 & \textbf{2D Model} & \textbf{3D Model} & \textbf{Hybrid Models}  & \textbf{(2+1)D Model}\\
\hline
Multi GPU & Single-Class Seg. & Single-Class Seg. & Single-Class Seg. & Single-Class Seg.\\
 & Multi-Class Seg. & Multi-Class Seg. & Multi-Class Seg. & Multi-Class Seg.\\
\hline
Single GPU & Single-Class Seg. & Single-Class Seg. & Single-Class Seg. & Single-Class Seg.\\
 & Multi-Class Seg. & Multi-Class Seg. & Multi-Class Seg. & Multi-Class Seg.\\
\end{tabular}
\end{center}
  \caption[Approached comparisons in this thesis]{Table of all approaches and models used in this project. With the different models the importance of spatial information for semantic segmentation of the LV should be evaluated. Besides the spatial information, the difference of segmenting the epi- and endocardium together or of its own will be analyzed. All this will be additionally compared if a parallel calculation of the models can benefit the training phase.}
  \label{tab:comparisons}
\end{table}

In the next chapter the used dataset and the model topologies are further described, that were used for a comprehensive comparison between the different approaches.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../thesis_main"
%%% End:
