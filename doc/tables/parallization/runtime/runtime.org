|       |    2D |    3D |  2-3D |  3-2D |
|-------+-------+-------+-------+-------|
| MC-MG | 46:06 | 59:08 | 47:31 | 45:22 |
| MC-SG | 50:17 | 53:30 | 53:04 | 51:51 |
|-------+-------+-------+-------+-------|
| SC-MG | 45:36 | 45:10 | 44:34 | 46:34 |
| SC-SG | 50:42 | 54:44 | 52:21 | 52:09 |
